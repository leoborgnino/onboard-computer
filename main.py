#########################################################################################
##  Main smart car                                                                      #
##                                                                                      #
##  execute in terminal: python main.py sys.argv[1] sys.argv[2] sys.argv[3]             #
##                                                                                      #
##  sys.argv[1] = name of the serial port to use. Example: /dev/ttyUSB0                 #
##  sys.argv[2] = name of the image to import and use in the planing                    #
##  sys.argv[3] = Simulator Mode enable/disable ( SIM_MODE )                            #
#########################################################################################

import time
import sys
import threading
import ThreadHandler
import graphic
import Comunicacion
import Planing
import dataRequest
import Vehiculo
import misc 

BAUD_RATE = 115200

# Fixme sacar esto por algo mas prolijo
if (sys.argv[3] != "SIM_MODE"):
    SIM_MODE = 0
else:
    SIM_MODE = 1

if (sys.argv[4] != "GRAPHIC_MODE"):
    GRAPHIC_MODE = 0
else:
    GRAPHIC_MODE = 1

# Comunicacion
com = Comunicacion.Comunicacion(sys.argv[1],BAUD_RATE,SIM_MODE)
hilo_recepcion = ThreadHandler.ThreadHandler(com.receive, "Hilo de recepcion")
hilo_envio = ThreadHandler.ThreadHandler(com.send, "Hilo de envio")

# Vehiculo
status_vehiculo = Vehiculo.Vehiculo()
getdata = dataRequest.dataRequest(com,status_vehiculo)

# Graficos
if (GRAPHIC_MODE):
    grafico = graphic.grafico(getdata)
    hilo_grafico = ThreadHandler.ThreadHandler(grafico.run, "Hilo Grafico")
else:
    hilo_grafico = None

misc.open_threads(hilo_recepcion,hilo_envio,hilo_grafico)

# Planning
pl = Planing.Planing(com,sys.argv[2],getdata)
pl.run()

# Esc = salir
with misc.NonBlockingConsole() as nbc:
    while True:
        if nbc.get_data() == '\x1b':  # x1b is ESC
            break
        getdata.obtener_datos(7)
        time.sleep(0.1)

# Hard STOP
datos = [chr(108)]
com.txfifo(datos,0)
print "Programa terminado"

#time.sleep(5)
misc.close_threads(hilo_recepcion,hilo_envio,hilo_grafico)

misc.plot_past_ego ()
