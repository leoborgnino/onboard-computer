import time
from Obj_Head import Obj_Head
import struct
import math
from enum import Enum

class ID_Request(Enum):
        ACCEL = 0
        GYRO  = 1
        IMU   = 2
        MOV   = 3
        ULTR  = 4
        GPS   = 5
        FSM   = 6
        ALL   = 7
        

class dataRequest(Obj_Head):        
        def __init__(self,com,vehicle_status):
                Obj_Head.__init__(self,com,self.save)
                self.error_reading    =  0
                self.TIME_OUT         = 10
                self.__sensor_try_thr =  5
                self.__sensor_try_cnt =  0
                self.__sensor_ok      = [1,1] # Sensor 0: Steer, Sensor 1: General Vehicle
                self.__flag_datos     =  0
                self.__arreglo_datos  = []
                self.__id_request     = ID_Request.ALL
                self.vehicle_status   = vehicle_status

        def save(self,datos):
                i = 0
                self.__arreglo_datos = []
                try:
                    while(i < len(datos)):
                        cadena = ""
                        while(datos[i]) != 32:
                            cadena = cadena + chr(datos[i])
                            i += 1
                        if cadena != "":
		        	self.__arreglo_datos.append(cadena)
                        i += 1
                except IndexError as error:
                        print "Error leyendo data"
                        #print datos
                        self.error_reading = 1
                self.__flag_datos = 1

        def obtener_datos(self, id_request):
                self.id_request = id_request
                self.send( [chr(103)] + [chr(id_request)] )
                timeout = time.time() + self.TIME_OUT
                while not self.__flag_datos and time.time() < timeout:
                        pass
                self.__flag_datos = 0
                if (time.time() <= timeout and self.error_reading == 0):
                        if ( self.id_request == ID_Request.ACCEL.value or self.id_request == ID_Request.ALL.value  or self.id_request == ID_Request.IMU.value ):
                                self.vehicle_status.x_acel = self.__arreglo_datos[0]
                                self.vehicle_status.y_acel = self.__arreglo_datos[1]
                                self.vehicle_status.z_acel = self.__arreglo_datos[2]
                        if ( self.id_request == ID_Request.GYRO.value or self.id_request == ID_Request.ALL.value or self.id_request == ID_Request.IMU.value ):
                                self.vehicle_status.x_gyro = self.__arreglo_datos[0 + ( self.id_request == ID_Request.ALL.value or self.id_request == ID_Request.IMU.value ) * 3]
                                self.vehicle_status.y_gyro = self.__arreglo_datos[1 + ( self.id_request == ID_Request.ALL.value or self.id_request == ID_Request.IMU.value ) * 3]
                                self.vehicle_status.z_gyro = self.__arreglo_datos[2 + ( self.id_request == ID_Request.ALL.value or self.id_request == ID_Request.IMU.value ) * 3]
                        if ( self.id_request == ID_Request.MOV.value or self.id_request == ID_Request.ALL.value):
                                self.vehicle_status.velocidad_act     = self.__arreglo_datos[0 + (self.id_request == ID_Request.ALL.value) * 6]
                                self.vehicle_status.velocidad_ref     = self.__arreglo_datos[1 + (self.id_request == ID_Request.ALL.value) * 6]
                                self.vehicle_status.distancia_act     = self.__arreglo_datos[2 + (self.id_request == ID_Request.ALL.value) * 6]
                                self.vehicle_status.distancia_ref     = self.__arreglo_datos[3 + (self.id_request == ID_Request.ALL.value) * 6]
                                self.vehicle_status.z_gyro               = self.__arreglo_datos[4 + (self.id_request == ID_Request.ALL.value) * 6]
                                self.vehicle_status.z_gyro_ref           = self.__arreglo_datos[5 + (self.id_request == ID_Request.ALL.value) * 6]
                                self.vehicle_status.steer_x_acel      = self.__arreglo_datos[6 + (self.id_request == ID_Request.ALL.value) * 6]
                                self.vehicle_status.steer_y_acel      = self.__arreglo_datos[7 + (self.id_request == ID_Request.ALL.value) * 6]                                               
                                self.vehicle_status.steer_z_acel      = self.__arreglo_datos[8 + (self.id_request == ID_Request.ALL.value) * 6]
                                self.vehicle_status.steer_ref         = self.__arreglo_datos[9 + (self.id_request == ID_Request.ALL.value) * 6]
                        self.error_reading = 0
                        if (self.__sensor_try_cnt <= self.__sensor_try_thr ):
                                if ( self.vehicle_status.steer_z_acel == 0 ):
                                        self.__sensor_ok[0]      = 0
                                if ( self.vehicle_status.z_gyro == 0.0 ):
                                        self.__sensor_ok[1]      = 0                        
                                self.__sensor_try_cnt +=  1
                                if ( self.__sensor_try_cnt == (self.__sensor_try_thr+1) ):
                                        print("\033[1;37;40m **************************************  \n")
                                        print("\033[1;37;40m ********* Accel sensors test *********  \n")
                                        print("\033[1;37;40m **************************************  \n")
                                        if ( self.__sensor_ok[0] ):
                                                print("\033[1;32;40m Sensor 0: Steer [OK]  \n")
                                        else:
                                                print("\033[1;31;40m Sensor 0: Steer [FAIL]  \n")
                                        if ( self.__sensor_ok[1] ):
                                                print("\033[1;32;40m Sensor 0: Vehicle [OK]  \n")
                                        else:
                                                print("\033[1;31;40m Sensor 0:Vehicle [FAIL]  \n")
                                        print("\033[1;37;40m **************************************  \n")
                        else:
                                self.vehicle_status.inclinacion()
                                self.vehicle_status.print_datos()
                                self.vehicle_status.log_datos()
                else:
                        print "\n**********************"
                        print "Error reading car data"
                        print "**********************"

