import time
import struct
import math

class Vehiculo():
    def __init__(self):
        self.log_status_file = open ('log_file.log','w')
        # Accelerometer data
        self.x_acel     = 0.
        self.y_acel     = 0.
        self.z_acel     = 0.
        self.x_rotation = 0.
        self.y_rotation = 0.
        # Gyro data
        self.x_gyro     = 0.
        self.y_gyro     = 0.
        self.z_gyro     = 0.
        self.z_gyro_ref = 0.
        # Movement data
        self.velocidad_act     = 0.
        self.velocidad_ref     = 0.
        self.distancia_act     = 0.
        self.distancia_ref     = 0.
        # Steer
        self.steer_x_acel      = 0.
        self.steer_y_acel      = 0.
        self.steer_z_acel      = 0.
        self.steer_x_rotation  = 0.
        self.steer_y_rotation  = 0.
        self.steer_ref         = 0.
        # GPS
        # ULTR
        # FSM        
        
    def inclinacion(self): # FIXME Usar argumentos de entrada para limpiar codigo
        self.x_rotation = math.degrees(math.atan2(float(self.y_acel),math.sqrt((float(self.x_acel)*float(self.x_acel)) + (float(self.z_acel) * float(self.z_acel)))))
        self.y_rotation = -math.degrees(math.atan2(float(self.x_acel),math.sqrt((float(self.y_acel)*float(self.y_acel)) + (float(self.z_acel) * float(self.z_acel)))))
        self.steer_x_rotation = math.degrees(math.atan2(float(self.steer_y_acel),math.sqrt((float(self.steer_x_acel)*float(self.steer_x_acel)) + (float(self.steer_z_acel) * float(self.steer_z_acel)))))
        self.steer_y_rotation = -math.degrees(math.atan2(float(self.steer_x_acel),math.sqrt((float(self.steer_y_acel)*float(self.steer_y_acel)) + (float(self.steer_z_acel) * float(self.steer_z_acel)))))
        
    def print_datos(self):
        print "\nAcelerometro: \t\tGiroscopo: \t\tInclinacion:"
	print "--------------\t\t-----------\t\t------------"
        print "Eje x: %s \t\tEje x: %s \t\tEje x: %s" % (self.x_acel,self.x_gyro,self.x_rotation)
	print "Eje y: %s \t\tEje y: %s \t\tEje y: %s" % (self.y_acel,self.y_gyro,self.y_rotation)
        print "Eje z: %s \t\tEje z: %s" % (self.z_acel,self.z_gyro)
        print "Yaw act: %s Yaw ref: %s " % (self.z_gyro,self.z_gyro_ref)
        print "Steer act: %s  Steer ref: %s " % (self.steer_x_rotation,self.steer_ref)
        print "Velocidad act: %s Velocidad ref: %s " % (self.velocidad_act,self.velocidad_ref)
        print "Distancia act: %s Distancia ref: %s " % (self.distancia_act,self.distancia_ref)

    def log_datos(self):
        self.log_status_file.write("%s %s %s " % (self.x_acel,self.x_gyro,self.x_rotation))
        self.log_status_file.write("%s %s %s " % (self.y_acel,self.y_gyro,self.y_rotation))
        self.log_status_file.write("%s %s " % (self.z_acel,self.z_gyro))
        self.log_status_file.write("%s %s " % (self.z_gyro,self.z_gyro_ref))
        self.log_status_file.write("%s %s " % (self.steer_x_rotation,self.steer_ref))
        self.log_status_file.write("%s %s " % (self.velocidad_act,self.velocidad_ref))
        self.log_status_file.write("%s %s\n" % (self.distancia_act,self.distancia_ref))
