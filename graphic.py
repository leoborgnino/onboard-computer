import numpy as np
import freenect
import cv2
import time


class grafico:

    def __init__(self,getdata):
        self.getdata = getdata

    def run(self):
        #get a frame from RGB camera
        frame = self.get_video()
        #get a frame from depth sensor
        depth = self.get_depth()
        #display RGB image
        #cv2.imshow('RGB image',frame)
        #display depth image
        #cv2.imshow('Depth image',depth)
 
        # quit program when 'esc' key is pressed
        k = cv2.waitKey(100) & 0xFF
        if k == 27:
            cv2.destroyAllWindows()

    #function to get RGB image from kinect
    def get_video(self):
        array,_ = freenect.sync_get_video()
        array = cv2.cvtColor(array,cv2.COLOR_RGB2BGR)
        return array
 
    #function to get depth image from kinect
    def get_depth(self):
        array,_ = freenect.sync_get_depth()
        array = array.astype(np.uint8)
        return array
