from scipy import misc
from math import *
import random
from Obj_Head import Obj_Head
import plan
import time
import math
import numpy as np
import matplotlib.pyplot as plt
import sys
import select
import tty
import termios

SCALE_FACTOR  = 26 # Tile step
RAD_TO_DEG    = 180.0/math.pi
VELOCIDAD     = 100
LOG_COMMAND   = 1

class Planing(Obj_Head):

    def __init__(self,com,imagen,data_request):
        # Pesada Herencia
        Obj_Head.__init__(self,com,self.flag)
        # Se levanta el mapa y se obtiene el punto de inicio, final y matriz binarizada
        self.m_plan,self.init,self.goal = self.map_process(imagen)
        self.tita = 0 # Orientacion Inicial
        # A Star Algorithm
        self.path = plan.plan(self.m_plan,self.init,self.goal)
        self.stop = self.path.astar()
        self.path.smooth(0.5,0.15)
        # Answer from core-mcu
        self.__flag = 0
        self.commands_file = open('commands.log','w')
        # Data request object
        self.data_request = data_request
    
    def run(self):
        stop = False
        vector_phase = 0.
        vector_module = 0.
        path_hard = self.path.path
        path_soft = self.path.spath
        i = 0
        while (i < (len(path_soft)-1) and stop == False):
        #while (i < 1 and stop == False):
            # Change from cartesian points in matrix to mod/phase in order to perform CTE control
            vector_module, vector_phase = self.cart_to_pol(np.array( [path_soft[i+1][0] - path_soft[i][0],path_soft[i+1][1] - path_soft[i][1] ] ))
            # Send data to core-mcu
            self.send_move_command(vector_phase,vector_module,VELOCIDAD,LOG_COMMAND)
            # Wait core-mcu answer
            with NonBlockingConsole() as nbc:
                while not self.__flag :
                    if nbc.get_data() == '\x1b':  # x1b is ESC
                        stop = True
                        break
            i += 1
            self.__flag = 0
        # Stop Command            
        self.send_move_command(vector_phase,vector_module,0,LOG_COMMAND)
        print "STOP"
        # Plot grid and path
        x_soft = np.array(path_soft)[0:i,0]
        y_soft = np.array(path_soft)[0:i,1]
        x_grid = []
        y_grid = []
        for i in range(len(self.path.grid)):
            for j in range(len(self.path.grid[0])):
                if (self.path.grid[i][j] == 1):
                    y_grid.append(j)
                    x_grid.append(i)

        #plt.plot(x_grid,y_grid,'ro')
        #plt.plot(x_soft,y_soft,'bo-')
        #plt.show()

    ## flag: Method loggued in Scheduler. When datos from  
    ## core-mcu answers, the movement result flag function 
    ## processes it.                                       
        
    def flag(self,datos):
        #print "Dato answ: %d "%datos[0]
        self.flag_alarm = True if datos[0] == 49 else False
        self.__flag = 1

    ## Map processing.
    ## @param imagen: path a la imagen del mapa
    ## @return (m_plan, init, goal)
    ## m_plan: mapa en forma de matriz binarizada
    ## init  : punto de inicio del recorrido
    ## goal  : punto final del recorrido
                
    def map_process(self,imagen):
        m_imagen = misc.imread(imagen)
        m_plan = np.zeros((len(m_imagen),len(m_imagen[0])))
        for i in range(len(m_imagen)):
            for j in range(len(m_imagen[0])):
                if (m_imagen[i][j][0] > 200) and (m_imagen[i][j][1] > 200) and (m_imagen[i][j][2] > 200):
                    m_plan[i][j] = 0
                elif (m_imagen[i][j][0] < 200) and (m_imagen[i][j][1] < 200) and (m_imagen[i][j][2] < 200):
                    m_plan[i][j] = 1
                elif (m_imagen[i][j][0] > 200) and (m_imagen[i][j][1] < 100) and (m_imagen[i][j][2] < 100):
                    goal = [i,j]
                elif (m_imagen[i][j][0] < 100) and (m_imagen[i][j][1] > 200) and (m_imagen[i][j][2] < 100):
                    init = [i,j]
        for i in range(len(m_plan)):
            print m_plan[i]
        print "Inicio: %d %d" % (init[0],init[1])
        print "Final:  %d %d" % (goal[0],goal[1])
        return (m_plan,init,goal)

    ## Cartesian to polar
    ## @param vector: vector [x,y]
    ## @return (vector_module, vector_phase)
    ## vector_module: modulo del vector
    ## phase : fase del vector en grados

    def cart_to_pol(self, vector):
        vector_module = np.sqrt(np.dot(vector, vector))
        if (vector[0] >= -0.01 and vector[0] <= 0.01): # Singularity
            if (vector[1] >= 0.):
                vector_phase = vector_phase - 90.0/RAD_TO_DEG
            else:
                vector_phase = vector_phase + 90.0/RAD_TO_DEG
        else: # arctg doesnt take bother about cartesian quadrant
            if ( vector[0] >= 0. and vector[1] <= 0 ):
                vector_phase  = np.arctan(vector[1]/vector[0]) + 180.0/RAD_TO_DEG
            elif ( vector[0] <= 0. and vector[1] <= 0 ):
                vector_phase  = np.arctan(vector[1]/vector[0])
            elif ( vector[0] >= 0. and vector[1] >= 0 ):
                vector_phase  = np.arctan(vector[1]/vector[0]) - 180.0/RAD_TO_DEG
            elif ( vector[0] <= 0. and vector[1] >= 0 ):
                vector_phase  = np.arctan(vector[1]/vector[0])
        return (vector_module, vector_phase)
                
    ## Send Move Command
    ## @param vector_phase: orientacion
    ## @param vector_module: distancia a recorrer
    ## @param velocidad: velocidad en cm/s del recorrido
    ## @param log_command: si es 1 escribe un archivo de logueo del comando

    def send_move_command(self, vector_phase, vector_module, velocidad, log_command):
        datos = [chr(107)] + [chr(0) if vector_phase*RAD_TO_DEG >= 0 else chr(1)] + [chr(abs(int(vector_phase*RAD_TO_DEG)))] + [chr(int(vector_module*SCALE_FACTOR)&0xFF)] + [chr((int(vector_module*SCALE_FACTOR)&0xFF00)>>8)] + [chr(1)] + [chr(velocidad)]
        if (log_command):
            for h in range(len(datos)):
                self.commands_file.write("%d " % (ord(datos[h])))      
        print "Avanzar %f centimetros a %f grados" % (vector_module*SCALE_FACTOR,vector_phase*RAD_TO_DEG)      
        self.commands_file.write("\n")      
        self.send(datos)

class NonBlockingConsole(object):

    def __enter__(self):
        self.old_settings = termios.tcgetattr(sys.stdin)
        tty.setcbreak(sys.stdin.fileno())
        return self

    def __exit__(self, type, value, traceback):
        termios.tcsetattr(sys.stdin, termios.TCSADRAIN, self.old_settings)


    def get_data(self):
        if select.select([sys.stdin], [], [], 0) == ([sys.stdin], [], []):
            return sys.stdin.read(1)
        return False

