import select
import tty
import termios
import time
import sys
import matplotlib.pyplot as plt

class NonBlockingConsole(object):

    def __enter__(self):
        self.old_settings = termios.tcgetattr(sys.stdin)
        tty.setcbreak(sys.stdin.fileno())
        return self

    def __exit__(self, type, value, traceback):
        termios.tcsetattr(sys.stdin, termios.TCSADRAIN, self.old_settings)


    def get_data(self):
        if select.select([sys.stdin], [], [], 0) == ([sys.stdin], [], []):
            return sys.stdin.read(1)
        return False

def open_threads(hilo_recepcion,hilo_envio,hilo_grafico):
    hilo_recepcion.start()
    time.sleep(0.05)
    hilo_envio.start()
    time.sleep(0.05)
    if (hilo_grafico != None):
        hilo_grafico.start()
        time.sleep(0.05)

def close_threads(hilo_recepcion,hilo_envio,hilo_grafico):
    hilo_recepcion.stop_thread()
    time.sleep(0.05)
    hilo_envio.stop_thread()
    time.sleep(0.05)
    if (hilo_grafico != None):
        hilo_grafico.stop_thread()
        time.sleep(0.05)

def plot_past_ego ():
    file_status = open('log_file.log','r')
    lines = file_status.readlines()
    gyro_act = []
    gyro_ref = []
    dist_act = []
    dist_ref = []
    steer_act = []
    steer_ref = []
    vel_act = []
    vel_ref = []
    for i in lines:
        line = i.split()
        print line
        try:
            gyro_act.append(float(line[8]))
            gyro_ref.append(float(line[9]))
            steer_act.append(float(line[10]))
            steer_ref.append(float(line[11]))
            vel_act.append(float(line[12]))
            vel_ref.append(float(line[13]))
            dist_act.append(float(line[14]))
            dist_ref.append(float(line[15]))
        except:
            pass

    f, axarr = plt.subplots(4, sharex=True)
    axarr[0].plot(gyro_act)
    axarr[0].plot(gyro_ref)
    axarr[0].set_title('Gyro')
    axarr[1].plot(steer_act)
    axarr[1].plot(steer_ref)
    axarr[1].set_title('Steer')
    axarr[2].plot(vel_act)
    axarr[2].plot(vel_ref)
    axarr[2].set_title('Vel')
    axarr[3].plot(dist_act)
    axarr[3].plot(dist_ref)
    axarr[3].set_title('Dist')
        
    plt.show()
        
    
    
